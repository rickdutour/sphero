import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  peripheral: any = {};
  statusMessage: string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private ble: BLE,
              private toastCtrl: ToastController,
              private ngZone: NgZone) {

    let device = navParams.get('device');

    this.setStatus('Connecting to ' + device.name || device.id);

    this.ble.connect(device.id).subscribe(
      peripheral => this.onConnected(peripheral),
      peripheral => this.onDeviceDisconnected(peripheral)
    );
  }

  onConnected(peripheral) {
    console.log('Connected to: ');
    console.log( JSON.stringify(peripheral));
    
    this.ngZone.run(() => {
      this.setStatus('');
      this.peripheral = peripheral;

      console.log( 'Sting to Bytes: ' );
      
      console.log( JSON.stringify(this.stringToBytes('JEUH')) );
    });
  }

  onDeviceDisconnected(peripheral) {
    console.log('Disconnected from: ');
    console.log(peripheral);
    let toast = this.toastCtrl.create({
      message: 'The peripheral unexpectedly disconnected',
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

  // Disconnect peripheral when leaving the page
  ionViewWillLeave() {
    console.log('ionViewWillLeave disconnecting Bluetooth');
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
    )
  }

  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  sendCommand( peripheral  ){
    let opts = {
      sop2: 'FFh',
      did: peripheral.id,
      cid: 'cid',
      seq: '0',
      data: ''
    };

    this.ble.write( peripheral.id, peripheral.characteristics[6].service, peripheral.characteristics[6].characteristic, this.stringToBytes( opts ) ).then( result => {

    }, err => {

    });
  }

  setColor( deviceId, serviceUUID, characteristicUUID, value){
    this.ble.write( deviceId, serviceUUID, characteristicUUID, this.stringToBytes(value) ).then( result => {
      console.log( 'Write Success: ', result  );
      
    }, err => {
      console.log( 'Write Err!', err );
      
    })
  }

  stringToBytes(string) {
    let array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
        array[i] = string.charCodeAt(i);
     }
     return array.buffer;
 }
 
  // ASCII only
  bytesToString(buffer) {
     return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

}